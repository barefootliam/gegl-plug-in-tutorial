# gegl plug-in tutorial

Welcome! This repository has the files for a tutorial on writing GEGL plug-ins in C, to be used in GIMP. Actually you can also use them in the _gegl_ stand-alone progrm too, and in any other programs that use _gegl_.

GEGL is the GEneric Graphic Library, a graph-based engine for processing data. It’s used internally by the GNU Image Manipulation Program (GIMP).

You can read the tutorial here; the files are referenced in Part Three on writing a C plug-in.

[part one](http://barefootliam.blogspot.com/2022/10/gegl-plug-ins-for-gimp-part-one-using.html)
[part two](http://barefootliam.blogspot.com/2022/12/gegl-plug-ins-for-gimp-part-two-gegl.html)
[part three](https://barefootliam.blogspot.com/2023/01/gegl-plug-ins-for-gimp-part-three.html)

## What’s Here

In the Releases folder there are zip files of everything, and a single zip file that contains all the others.

In the plug-ins folder you can see the sample GEGL plug-ins for the tutorial.

## Contributing

Pull requests, patches, comments are very welcome.

## Acknowledgments

Many thanks to the GIMP and GEGL teams, espcially pippin for answering questions patiently,
and above all thanks to LinuxBeaver who agitated me to helping him write a bunch of plug-ins
and, eventually, writing this article.

## License

The plug-ins are distributes under GPL v3 or later.

The article and various text files and any sample images might find their way into the GIMP
documentation at some point, so any contributions must be appropriately licensed.  Thank you!

