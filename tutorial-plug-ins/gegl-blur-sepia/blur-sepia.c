/* This file is an image processing operation for GEGL
 *
 * GEGL is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL; if not, see <https://www.gnu.org/licenses/>.
 *
 * Copyright 2006 Øyvind Kolås <pippin@gimp.org>
 *           2022 Liam Quin <slave@fromoldbooks.org>
 */

#include "config.h"
#include <glib/gi18n-lib.h>

#ifdef GEGL_PROPERTIES

property_double (blur_radius, _("Radius for blur, in pixels"), 3.0)
    description (_("Strength of the blur effect: higher values give a more blurred result."))
    value_range (0.0, 65535) /* arbitrarily large number */
    ui_range(0.0, 35.0) /* for the slider in the user interface */

    /* NOTE: ui_range is the range of values users can enter interactively.
     * Do not allow users to enter values outside the value_range. GEGL checks for
     * this and will report an error (or in a development build, by default will
     * crash into a debugger) if you allow this.
     */

property_double (sepia_strength, _("Strength of the sepia effect"), 0.8)
    description (_("Strength of the sepia effect: higher values give a more blurred result."))
    value_range (0.0, 1.0)
    /* UI range is same as value range so no need to specify it here */



#else

#include <gegl-plugin.h>

#define GEGL_OP_META
#define GEGL_OP_NAME     blur_sepia
#define GEGL_OP_C_SOURCE blur-sepia.c

#include "gegl-op.h"

static void attach(GeglOperation *operation)
{
  GeglNode *gegl = operation->node;
  GeglNode *input, *output, *blur, *sepia;

  input    = gegl_node_get_input_proxy (gegl, "input");
  output   = gegl_node_get_output_proxy (gegl, "output");

  blur = gegl_node_new_child (gegl, "operation", "gegl:gaussian-blur", NULL);
  sepia = gegl_node_new_child (gegl, "operation", "gegl:sepia", NULL);

  gegl_node_link_many (input, blur, sepia, output, NULL);

  /* arrange to pass on our two properties to their respective nodes */

  gegl_operation_meta_redirect (operation, "blur-radius", blur, "std-dev-x");
  gegl_operation_meta_redirect (operation, "blur-radius", blur, "std-dev-y");
  gegl_operation_meta_redirect (operation, "sepia-strength", sepia, "scale");

}


/* "composition" is for testing: */
static const gchar *composition =
    "<?xml version='1.0' encoding='UTF-8'?>"
    "<gegl>"
    "<node operation='gegl:crop' width='200' height='200'/>"
    "<node operation='gegl:blur-sepia' />"
    "</gegl>";

static void
gegl_op_class_init (GeglOpClass *klass)
{
  GeglOperationMetaClass *operation_meta_class = GEGL_OPERATION_META_CLASS (klass);
  GeglOperationClass       *operation_class = GEGL_OPERATION_CLASS (klass);

  operation_class->attach = attach;

  gegl_operation_class_set_keys (operation_class,
    "title",          _("Blur and Sepia Example"),
    "name",           "gegl:blur-sepia",
    "categories",     "Artistic",
    "reference-hash", "deafbededeafbededeafbededeafbede",
    "description",  _("Sample GEGL operation for tutorial"),
    NULL);

}


#endif
