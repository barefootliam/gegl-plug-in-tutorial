/* This file is an image processing operation for GEGL
 *
 * GEGL is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL; if not, see <https://www.gnu.org/licenses/>.
 *
 * Copyright 2006 Øyvind Kolås <pippin@gimp.org>
 *           2022 Liam Quin (barefootliam) blur-sepia-simple
 */

#include "config.h"
#include <glib/gi18n-lib.h>

#ifdef GEGL_PROPERTIES

/* none */

#else

#include <gegl-plugin.h>

#define GEGL_OP_META
#define GEGL_OP_NAME     blur_sepia_simple
#define GEGL_OP_C_SOURCE blur-sepia-simple.c

#include "gegl-op.h"

static void attach(GeglOperation *operation)
{
  GeglNode *gegl = operation->node;
  GeglNode *input, *output, *blur, *sepia;

  input    = gegl_node_get_input_proxy (gegl, "input");
  output   = gegl_node_get_output_proxy (gegl, "output");

  blur = gegl_node_new_child (gegl, "operation", "gegl:gaussian-blur", NULL);
  sepia = gegl_node_new_child (gegl, "operation", "gegl:sepia", NULL);

  gegl_node_link_many (input, blur, sepia, output, NULL);

}


/* "composition" is for testing: */
static const gchar *composition =
    "<?xml version='1.0' encoding='UTF-8'?>"
    "<gegl>"
    "<node operation='gegl:crop' width='200' height='200'/>"
    "<node operation='gegl:blur-sepia-simple' />"
    "</gegl>";

static void
gegl_op_class_init (GeglOpClass *klass)
{
  GeglOperationClass       *operation_class = GEGL_OPERATION_CLASS (klass);

  operation_class->attach = attach;

  gegl_operation_class_set_keys (operation_class,
    "reference-composition", composition,
    "title",          _("Blur and Sepia Simple Example"),
    "name",           "gegl:blur-sepia-simple",
    "categories",     "Artistic",
    "reference-hash", "deafbededeafbededeafbededeafbede",
    "description",  _("Sample very simple GEGL operation for tutorial"),
    NULL);

}


#endif
